package com.jasper.hoihoi;

import java.util.ArrayList;
import java.util.List;

/**
 * This class implements mortgage calculations.
 * 
 * Jasper based this on
 * https://github.com/rpj911/LeetCode_algorithm/blob/master/MortgageCalculator.java
 */

public class MortgageCalculator {

	/**
	 * The interest rate for the mortgage in fraction (not %)
	 */
	private double interestRate;
	/**
	 * The mortgage term in years
	 */
	private int termInYears;
	/**
	 * The total amount of loan
	 */
	private int loanAmount;
	/**
	 * The amount to be paid monthly
	 */
	private double monthlyPayment;

	/**
	 * Initialize a mortgage calculator with the given mortgage parameters
	 * 
	 * @param loanAmount     total amount of loan
	 * @param termInYears    term of loan in years
	 * @param interestRate   loan interest rate, 5.6% = 5.6	
	 */
	public MortgageCalculator(int loanAmount, int termInYears, double interestRate) {
		// Check some reasonable bounds on the input
		if ((loanAmount <= 0) || (loanAmount > 100000000)) {
			throw new IllegalArgumentException("Loan amount too large or small");
		}
		if ((termInYears <= 0) || (termInYears > 100)) {
			throw new IllegalArgumentException("Term in years too large or small");
		}
		if (interestRate <= 0) {
			throw new IllegalArgumentException("Interest rate cannot be 0 or less");
		}
		if (interestRate > 50) {
			throw new IllegalArgumentException("Interest rate is too high, please get a better rate at <mortgage ad here> ;)");
		}
		
		// Copy locally
		this.loanAmount = loanAmount;
		this.termInYears = termInYears;
		this.interestRate = interestRate / 100;
		this.monthlyPayment = calculateMonthlyPayment();
	}
	
	/**
	 * Calculate monthly balance for the term of a loan
	 * 
	 * @return a list of monthly balances after payment
	 */
	public List<Double> calcMonthlyBalance() {
		double monthlyRate = interestRate / 12.0; 
		int termInMonths = termInYears * 12;

		// Loop through the term of the loan tracking the balance
		List<Double> balances = new ArrayList<Double>(termInMonths);

		double balance = loanAmount;
		for (int i = 0; i < termInMonths; i++) {

			// Add interest to the balance
			balance += (balance * monthlyRate);

			// Subtract the monthly payment
			balance -= getMonthlyPayment();

			// Add running balance
			balances.add((Double) balance);
		}

		return balances;
	}

	/**
	 * Calculates the monthly payment for a specified loan. This assumes input checks have already been performed. GIGO.
	 * 
	 * @return monthly payment
	 */
	private double calculateMonthlyPayment() {

		// Monthly interest rate
		// is the yearly rate divided by 12

		double monthlyRate = interestRate / 12.0;

		// The length of the term in months
		// is the number of years times 12

		int termInMonths = termInYears * 12;

		// Calculate the monthly payment
		// Typically this formula is provided so
		// we won't go into the details

		// The Math.pow() method is used
		// to calculate values raised to a power

		double monthlyPayment = (loanAmount * monthlyRate) / (1 - Math.pow(1 + monthlyRate, -termInMonths));

		return monthlyPayment;
	}

	/**
	 * @return the monthlyPayment
	 */
	public double getMonthlyPayment() {
		return monthlyPayment;
	}

}