package com.jasper.hoihoi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main application class, used for starting the Spring application and
 * MainController.
 * 
 * from: https://github.com/spring-guides/gs-spring-boot.git
 * 
 * @author jasper
 *
 */
@SpringBootApplication
public class Application {

	/**
	 * Start the main Spring application
	 * @param args Command line args to pass to SpringApplication
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
