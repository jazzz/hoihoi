package com.jasper.hoihoi;

import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

import java.text.NumberFormat;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Main controller running the mortgage application. 
 * 
 *  from: https://github.com/spring-guides/gs-spring-boot.git
 *  
 * @author jasper
 *
 */
@RestController
public class MainController {

	/**
	 * This method is to test the web server 
	 * 
	 * @return a test string
	 */
	@RequestMapping("/test")
	public String test() {
		return "Hoi hoi world!";
	}
	
	/**
	 * This method serves the main page.
	 * 
	 * @return the initial form used to enter mortgage details.
	 */
	@RequestMapping("/")
	public String form() {
		// Simple UI
		return 	"<form action=\"/mortgage\" method=\"GET\">\n" + 
				"	Loan amount:<input name=\"loanAmount\" value=\"500000\" /> <br /> \n" + 
				"    Term in years:<input name=\"termInYears\" value=\"30\" /> <br /> \n" + 
				"    Interest rate:<input name=\"interestRate\" value=\"4.2\" /> <br /> \n" + 
				"	<button type=\"submit\" value=\"Submit\">Submit</button>\n" + 
				"</form>"; 
	}

	/**
	 * This method wraps the mortgage calculator. It takes in the mortgage parameters
	 * and renders the output as HTML.
	 * 
	 * @param loanAmount     total amount of loan
	 * @param termInYears    term of loan in years
	 * @param interestRate   loan interest rate, 5.6% = 5.6
	 * @return HTML formatted mortgage info
	 */
	@RequestMapping("/mortgage")
	public String mortgage(@RequestParam int loanAmount, @RequestParam int termInYears, @RequestParam double interestRate) {
		// Fast string appender
		StringBuilder sb = new StringBuilder();
        
		// Currency formatted
		NumberFormat nf = NumberFormat.getCurrencyInstance();
        
		MortgageCalculator mortgage = new MortgageCalculator(loanAmount, termInYears, interestRate);
		
		// Get balances and monthly payment
		List<Double> balances = mortgage.calcMonthlyBalance();
		double monthly = mortgage.getMonthlyPayment();
		
		// Banner
		sb.append("Mortgage balance per month, with monthly payment of ");
		sb.append(escapeHtml(nf.format(mortgage.getMonthlyPayment())));
		sb.append(":<br/>");
		
		// Numbered list
		sb.append("<ol>");
		
		// Create output string
		for (Double balance : balances) {
			sb.append("<li>" + escapeHtml(nf.format(balance)) + "</li>");
		}
		
		sb.append("</ol>");
		
		//System.err.println(sb);
		
		return sb.toString();
	}
}
