package com.jasper.hoihoi;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

/**
 * Tests for the main controller. Uses MockMvc to bypass running a webserver
 * and getting better error feedback.
 * 
 * from: https://github.com/spring-guides/gs-spring-boot.git
 * 
 * @author jasper
 */
@SpringBootTest
@AutoConfigureMockMvc
public class MainControllerTest {

	@Autowired
	private MockMvc mvc;

	/**
	 * Test if the simple banner works
	 * @throws Exception 
	 */
	@Test
	public void getHello() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/test").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().string(equalTo("Hoi hoi world!")));
	}
	
	/**
	 * Test if the form works
	 * @throws Exception 
	 */
	@Test
	public void getForm() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	/**
	 * Tests whether missing or malformed parameters result in a HTTP bad request error
	 * 
	 * @throws Exception
	 */
	@Test
	public void mortgageParamMissing() throws Exception {
		// Test for missing and invalid parameters
		String[] badUrls = {
			"/mortgage", 
			"/mortgage?termInYears=10&interestRate=4.3", 
			"/mortgage?loanAmount=10&interestRate=4.3", 
			"/mortgage?loanAmount=10&termInYears=10", 
			"/mortgage?loanAmount=9.9&termInYears=10&interestRate=4.3", 
			"/mortgage?loanAmount=10&termInYears=9.9&interestRate=4.3", 
			"/mortgage?loanAmount=10&termInYears=10&interestRate=a"
			};
			
		// Loop over all "bad" urls expect a bad request result for each
		for (String badUrl : badUrls) {
			mvc.perform(MockMvcRequestBuilders.get(badUrl).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
		}
	}
	
	/**
	 * Test that the happy flow works without errors.
	 * 
	 * @throws Exception
	 */
	@Test
	public void mortgageHappy() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/mortgage?loanAmount=500000&termInYears=30&interestRate=4.2").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
}
