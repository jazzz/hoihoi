package com.jasper.hoihoi;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

public class MortgageCalculatorTest {

	/**
	 * Test mortgage calculations are correct.
	 * 
	 * Verified with: - Excel sheet from
	 * https://www.mortgagecalculator.org/download/excel.php -
	 * https://www.mortgagecalculatorplus.com/calculator/
	 */
	@Test
	public void mortgageTest1() {
		double[] verified = { 479993.00, 459915.98, 439768.69, 419550.88, 399262.31, 378902.73, 358471.89, 337969.55,
				317395.44, 296749.33, 276030.96, 255240.07, 234376.41, 213439.73, 192429.77, 171346.28, 150188.99,
				128957.65, 107652.01, 86271.79, 64816.75, 43286.61, 21681.11, 0.00 };
		int loanAmount = 500000;
		int termInYears = 2;
		double interestRate = 4.2;
		int precision = 100;
		double monthly = 21757.00;

		mortgageTest(loanAmount, termInYears, interestRate, monthly, verified, precision);
	}

	/**
	 * Test mortgage calculations are correct.
	 * 
	 * Verified with: - Excel sheet from
	 * https://www.mortgagecalculator.org/download/excel.php -
	 * https://www.mortgagecalculatorplus.com/calculator/
	 */
	@Test
	public void mortgageTest2() {
		double[] verified = { 113472.75, 103435.09, 93342.73, 83195.37, 72992.70, 62734.42, 52420.24, 42049.85,
				31622.94, 21139.20, 10598.32, 0.00 };
		int loanAmount = 123456;
		int termInYears = 1;
		double interestRate = 6.54;
		int precision = 100;
		double monthly = 10656.08;

		mortgageTest(loanAmount, termInYears, interestRate, monthly, verified, precision);
	}

	/**
	 * Test that the mortgage calculations match the verified list.
	 * 
	 * @param loanAmount     total amount of loan
	 * @param termInYears    term of loan in years
	 * @param interestRate   loan interest rate, 5.6% = 5.6
	 * @param verified       a list of correct monthly balances
	 * @param precision      the precision of the list. 100 means it's precise to the cent, 1 means it's precise to the dollar/euro/...
	 */
	private void mortgageTest(int loanAmount, int termInYears, double interestRate, double monthly, double[] verified, int precision) {
		MortgageCalculator mortgage = new MortgageCalculator(loanAmount, termInYears, interestRate);
		List<Double> test = mortgage.calcMonthlyBalance();
		assertEquals(verified.length, test.size());
		assertPrecision(monthly, mortgage.getMonthlyPayment(), precision);

		for (int i = 0; i < verified.length; i++) {
			assertPrecision(verified[i], test.get(i), precision);
		}
	}
	
	/** Checks whether two doubles, rounded to precision, are equal. 
	 *
	 * @param a first double
	 * @param b second double
	 * @param precision 100 means it's precise to 1/100
	 */
	private static void assertPrecision(double a, double b, int precision) {
		assertEquals((double)(Math.round(a * precision))/precision, 
				     (double)(Math.round(b * precision))/precision); // Use precision 
	}
}
